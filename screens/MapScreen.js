import * as React from "react";
import {
  StyleSheet,
  Image,
  Text,
  SafeAreaView,
  View,
  ScrollView,
  Platform,
  StatusBar,
  Dimensions,
} from "react-native";
import Menu from "./Menu";
import MapView, { Callout } from "react-native-maps";
import { Marker } from "react-native-maps";

export default MapScreen = ({ navigation }) => {
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.row}>
          <Menu navigation={navigation} />
          <View>
            <Image
              style={styles.logo}
              source={require("../assets/HaastivalLogoAlpha.png")}
            />
          </View>
          <Back navigation={navigation} />
        </View>
        <Text style={styles.mapTitle}>Our Map</Text>
        <MapView
          mapType="satellite"
          style={styles.map}
          initialRegion={{
            latitude: 55.175847207919034,
            longitude: 10.200477344664716,
            latitudeDelta: 0.0022,
            longitudeDelta: 0.001,
          }}
        >
          <Marker
            pinColor="#0FC500"
            coordinate={{
              latitude: 55.174188,
              longitude: 10.199409,
            }}
          >
            <Callout>
              <Text>CORONA INDTJEKNING</Text>
            </Callout>
          </Marker>
          <Marker
            pinColor="#08FF00"
            coordinate={{
              latitude: 55.174683,
              longitude: 10.199736,
            }}
          >
            <Callout>
              <Text>HAVE</Text>
            </Callout>
          </Marker>
          <Marker
            pinColor="#08FF00"
            coordinate={{
              latitude: 55.174639,
              longitude: 10.200315,
            }}
          >
            <Callout>
              <Text>HAVE</Text>
            </Callout>
          </Marker>
          <Marker
            coordinate={{
              latitude: 55.17468341943661,
              longitude: 10.200030848060457,
            }}
          >
            <Callout>
              <Text>STALD</Text>
            </Callout>
          </Marker>
          <Marker
            coordinate={{
              latitude: 55.174864,
              longitude: 10.200374,
            }}
          >
            <Callout>
              <Text>LADE</Text>
            </Callout>
          </Marker>
          <Marker
            coordinate={{
              latitude: 55.174923,
              longitude: 10.199908,
            }}
          >
            <Callout>
              <Text>BEBOELSE</Text>
            </Callout>
          </Marker>
          <Marker
            pinColor="#00FFE0"
            coordinate={{
              latitude: 55.174921,
              longitude: 10.200128,
            }}
          >
            <Callout>
              <Text>TOILETVOGN</Text>
            </Callout>
          </Marker>
          <Marker
            coordinate={{
              latitude: 55.17501,
              longitude: 10.200482,
            }}
          >
            <Callout>
              <Text>OVERDÆKKET BAR OMRÅDE</Text>
            </Callout>
          </Marker>
          <Marker
            pinColor="#CBFF6B"
            coordinate={{
              latitude: 55.175127,
              longitude: 10.200513,
            }}
          >
            <Callout>
              <Text>SALGSTELTE</Text>
            </Callout>
          </Marker>
          <Marker
            pinColor="#CBFF6B"
            coordinate={{
              latitude: 55.175253,
              longitude: 10.200551,
            }}
          >
            <Callout>
              <Text>SALGSTELTE</Text>
            </Callout>
          </Marker>
          <Marker
            pinColor="#CBFF6B"
            coordinate={{
              latitude: 55.17506,
              longitude: 10.200216,
            }}
          >
            <Callout>
              <Text>SALGSTELTE</Text>
            </Callout>
          </Marker>
          <Marker
            pinColor="#AA38FE"
            coordinate={{
              latitude: 55.175202,
              longitude: 10.200329,
            }}
          >
            <Callout>
              <Text>ÅBENT SIDDE/SPISE OMRÅDE</Text>
            </Callout>
          </Marker>
          <Marker
            pinColor="yellow"
            coordinate={{
              latitude: 55.175336,
              longitude: 10.200015,
            }}
          >
            <Callout>
              <Text>SCENE</Text>
            </Callout>
          </Marker>
          <Marker
            pinColor="orange"
            coordinate={{
              latitude: 55.175206,
              longitude: 10.199894,
            }}
          >
            <Callout>
              <Text>CIRKUS MANEGE</Text>
            </Callout>
          </Marker>
        </MapView>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffb84d",
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },
  logo: {
    width: 150,
    height: 170,
    top: 10,
    right: "65%",
    marginBottom: 50,
  },
  row: {
    flexDirection: "row-reverse",
  },
  mapTitle: {
    backgroundColor: "#ff471a",
    fontSize: 50,
    fontWeight: "bold",
    fontStyle: "italic",
    textAlign: "center",
    borderRadius: 20,
    borderWidth: 2,
    marginHorizontal: 5,
    overflow: "hidden",
    marginBottom: 30,
    fontFamily: "Overlock",
  },
  map: {
    alignSelf: "center",
    width: Dimensions.get("window").width * 0.9,
    height: 450,
    borderColor: "#ff471a",
    borderWidth: 2,
  },
});
