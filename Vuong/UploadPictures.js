import React from "react";
import {
  StyleSheet,
  Text,
  View,
  ActivityIndicator,
  SafeAreaView,
  Dimensions,
  Image,
  StatusBar,
} from "react-native";
import { Constants } from "expo";
import Menu from "../screens/Menu";

import { WebView } from "react-native-webview";
export default UploadPictures = ({ navigation }) => {
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.row}>
        <Menu navigation={navigation} />
        <View>
          <Image
            style={styles.logoTitle}
            source={require("../assets/HaastivalLogoAlpha.png")}
          />
        </View>
      </View>
      <WebView
        source={{
          uri: "https://orderlando.com/SharedPictures/EditSharedPictures?userkey=BJ9GDU",
        }}
        style={{
          width: Dimensions.get("screen").width,
          height: Dimensions.get("screen").height,
          backgroundColor: "blue",
        }}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#ffb84d",
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },
  row: {
    flexDirection: "row-reverse",
  },
  logoTitle: {
    width: 150,
    height: 170,
    top: 10,
    right: "65%",
    marginBottom: 50,
  },
});
