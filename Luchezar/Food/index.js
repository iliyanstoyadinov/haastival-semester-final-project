import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  SafeAreaView,
  StatusBar,
} from "react-native";
import Menu from "../../screens/Menu";
import Back from "../../screens/Back";
import { useNavigation } from "@react-navigation/native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffb84d",
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },
  logo: {
    width: 380,
    height: 480,
    marginLeft: "4%",
    marginTop: "10%",
  },
  logo1: {
    width: 160,
    height: 160,
    marginLeft: "11%",
    marginTop: "10%",
    marginBottom: "10%",
  },
  title: {
    backgroundColor: "#ff471a",
    fontSize: 50,
    textAlign: "center",
    borderRadius: 20,
    borderWidth: 2,
    marginHorizontal: 5,
    overflow: "hidden",
    marginBottom: 20,
    fontFamily: "Overlock",
  },
  row: {
    flexDirection: "row-reverse",
  },
  logoTitle: {
    width: 150,
    height: 170,
    top: 10,
    right: "65%",
    marginBottom: 50,
  },
});

const Food = () => {
  const navigation = useNavigation();
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.row}>
          <Menu navigation={navigation} />
          <View>
            <Image
              style={styles.logoTitle}
              source={require("../../assets/HaastivalLogoAlpha.png")}
            />
          </View>
          <Back navigation={navigation} />
        </View>
        <Text style={styles.title}>Menus</Text>
        
        <Image style={styles.logo} source={require("./cocktail.png")} />
        <Image style={styles.logo} source={require("./shots.png")} />
       
        <View style = {[styles.container, { flexDirection: "row", flexWrap: 'wrap'}]}>
        <TouchableOpacity
          style={styles.margin}
          activeOpacity={0.5}
          onPress={() => alert("Image Clicked!!!")}
        >
          <Image style={styles.logo1} source={require("./coupon-25.png")} />
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.margin}
          activeOpacity={0.5}
          onPress={() => alert("Image Clicked!!!")}
        >
          <Image style={styles.logo1} source={require("./coupon-50.png")} />
        </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default Food;
