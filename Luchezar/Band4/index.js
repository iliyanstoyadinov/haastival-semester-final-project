import React, { Component } from "react";
import { View, ScrollView, Image, StyleSheet, Text } from "react-native";

const Band4 = () => {
  return (
    <View style={styles.container}>
      <ScrollView>
        <Text style={styles.textst}>Sharks On Board Ny </Text>
        <Image style={styles.logo} source={require("./Band4.png")} />
        <Text style={styles.textsm}>
          Sharks On Board er en dansk ny-cirkus produktion, som egner sig til
          børn og barnlige sjæle i alle aldre. Forestillingen udspiller sig i et
          univers hvor alt kan ske og kun fantasien sætter grænser. En verden
          hvor publikum kan læne sig tilbage og nyde en akrobatisk rejse fuld af
          historier, overraskelser og hæsblæsende Cirkus kunstner.
        </Text>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: "10%",
    backgroundColor: "#ffb84d",
    paddingBottom: "10%",
  },
  logo: {
    width: 320,
    height: 280,
    marginLeft: "10%",
    marginTop: "10%",
    marginBottom: "2%",
  },
  textst: {
    fontFamily: "Overlock",
    fontSize: 40,
    textAlign: "center",
  },
  textsm: {
    margin: "3%",
    fontWeight: "normal",
    fontSize: 20,
    textAlign: "justify",
    fontFamily: "Overlock",
  },
});

export default Band4;
