import React from "react";
import { View, StyleSheet, SafeAreaView, Pressable, Text } from "react-native";
import { Feather } from "@expo/vector-icons";

export default Back = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <SafeAreaView>
        <Pressable
          style={styles.back}
          onPress={() => navigation.navigate("Home")}
        >
          <Feather name="arrow-left-circle" size={30} color={"black"} />
          <Text style={styles.textHome}>Home</Text>
        </Pressable>
      </SafeAreaView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  back: {
    marginTop: 10,
    marginHorizontal: 5,
    flexDirection: "row",
  },
  textHome: {
    marginTop: 3,
    fontSize: 20,
  },
});
