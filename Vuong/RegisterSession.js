import React from "react";
import {
  StyleSheet,
  Text,
  View,
  ActivityIndicator,
  SafeAreaView,
  Dimensions,
  Image,
  StatusBar,
} from "react-native";
import { Constants } from "expo";
import Menu from "../screens/Menu";
import Back from "../screens/Back";

import { WebView } from "react-native-webview";
export default RegisterSession = ({ navigation }) => {
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.row}>
        <Menu navigation={navigation} />
        <View>
          <Image
            style={styles.logoTitle}
            source={require("../assets/HaastivalLogoAlpha.png")}
          />
        </View>
        <Back navigation={navigation} />
      </View>
      <WebView
        source={{ uri: "https://orderlando.com/SessionRegister/Index" }}
        style={{
          width: Dimensions.get("screen").width,
          height: Dimensions.get("screen").height,
          backgroundColor: "blue",
        }}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#ffb84d",
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },
  row: {
    flexDirection: "row-reverse",
  },
  logoTitle: {
    width: 150,
    height: 170,
    top: 10,
    right: "70%",
  },
});
