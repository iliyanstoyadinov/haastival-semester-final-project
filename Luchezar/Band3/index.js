import React, { Component } from "react";
import {
  Linking,
  View,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
} from "react-native";

const Band3 = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.textst}>HOLMEN HUSTLERS </Text>
      <Image style={styles.logo} source={require("./Band3.png")} />
      <Text style={styles.textsm}>Social media:</Text>
      <View style={[styles.fb, { flexDirection: "row", flexWrap: "wrap" }]}>
        <TouchableOpacity
          activeOpacity={0.5}
          onPress={() => Linking.openURL(url)}
        >
          <Image style={styles.log} source={require("./fb.webp")} />
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={0.5}
          onPress={() => Linking.openURL(url1)}
        >
          <Image style={styles.log} source={require("./sp.png")} />
        </TouchableOpacity>
      </View>
    </View>
  );
};
const url = "https://www.facebook.com/HolmenHustlers";
const url1 =
  "https://open.spotify.com/album/7LT3jAReDjXqaqjJvju1Xx?highlight=spotify:track:0hHgTn4f0aCbLTrqC5Ljon";
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: "10%",
    backgroundColor: "#ffb84d",
    paddingBottom: "10%",
  },
  logo: {
    width: 360,
    height: 320,
    marginLeft: "10%",
    marginTop: "10%",
  },
  textst: {
    fontFamily: "Overlock",
    fontSize: 45,
    textAlign: "center",
  },
  log: {
    width: 60,
    height: 60,
    marginLeft: "5%",
    marginTop: "5%",
  },
  fb: {
    margin: "5%",
  },
  textsm: {
    marginLeft: "2%",
    marginTop: "2%",
    fontFamily: "Overlock",
    fontSize: 30,
    textAlign: "left",
  },
});

export default Band3;
