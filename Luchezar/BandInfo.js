import React from "react";
import {
  Text,
  View,
  Image,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  SafeAreaView,
  StatusBar,
} from "react-native";
import Menu from "../screens/Menu";
import Back from "../screens/Back";
import { useNavigation } from "@react-navigation/native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    backgroundColor: "#ffb84d",
  },

  margin: {
    marginBottom: 60,
  },
  logo: {
    width: 320,
    height: 280,
    alignSelf: "center",
  },
  label: {
    fontSize: 38,
    textAlign: "center",
    marginBottom: 10,
    fontFamily: "Overlock",
  },
  title: {
    backgroundColor: "#ff471a",
    fontSize: 48,
    textAlign: "center",
    borderRadius: 20,
    borderWidth: 2,
    marginHorizontal: 5,
    overflow: "hidden",
    marginBottom: 50,
    fontFamily: "Overlock",
  },
  row: {
    flexDirection: "row-reverse",
  },
  logoTitle: {
    width: 150,
    height: 170,
    top: 10,
    right: "65%",
    marginBottom: 50,
  },
});

const DisplayAnImage = () => {
  const navigation = useNavigation();
  return (
    <SafeAreaView style={[styles.container]}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.row}>
          <Menu navigation={navigation} />
          <View>
            <Image
              style={styles.logoTitle}
              source={require("../assets/HaastivalLogoAlpha.png")}
            />
          </View>
          <Back navigation={navigation} />
        </View>
        <Text style={styles.title}>Band Information</Text>
        <TouchableOpacity
          style={styles.margin}
          activeOpacity={0.5}
          onPress={() => navigation.navigate("Band1")}
        >
          <Text style={styles.label}>ALIVAN </Text>
          <Image style={styles.logo} source={require("./Band1.png")} />
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.margin}
          activeOpacity={0.5}
          onPress={() => navigation.navigate("Band2")}
        >
          <Text style={styles.label}>Bitch Muchannon </Text>
          <Image style={styles.logo} source={require("./Band2.png")} />
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.margin}
          activeOpacity={0.5}
          onPress={() => navigation.navigate("Band3")}
        >
          <Text style={styles.label}>HOLMEN HUSTLERS </Text>
          <Image style={styles.logo} source={require("./Band3.png")} />
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.margin}
          activeOpacity={0.5}
          onPress={() => navigation.navigate("Band4")}
        >
          <Text style={styles.label}>Sharks On Board Ny </Text>
          <Image style={styles.logo} source={require("./Band4.png")} />
        </TouchableOpacity>
      </ScrollView>
    </SafeAreaView>
  );
};

export default DisplayAnImage;
