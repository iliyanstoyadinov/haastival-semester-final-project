export default [
  {
    id: "1",
    title: "Niels Lundgaard",
    image: require("../assets/Niels.jpg"),
  },
  {
    id: "2",
    title: "Maria Lundgaard",
    image: require("../assets/Maria.jpg"),
  },
  {
    id: "3",
    title: "Uundværlige Palle Notlev",
    image: require("../assets/Palle2.jpg"),
  },
];
