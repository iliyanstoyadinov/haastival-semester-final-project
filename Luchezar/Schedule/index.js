import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  SafeAreaView,
  Image,
  StatusBar,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import Menu from "../../screens/Menu";
import Back from "../../screens/Back";

const Schedule = () => {
  const navigation = useNavigation();

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.row}>
          <Menu navigation={navigation} />
          <View>
            <Image
              style={styles.logo}
              source={require("../../assets/HaastivalLogoAlpha.png")}
            />
          </View>
          <Back navigation={navigation} />
        </View>
        <Text style={styles.title}>Schedule</Text>
        <View style={styles.middle}>
          <Text style={styles.baseText}>
            Kl. 11.00{" "}
            <Text style={styles.innerText}>
              Vi åbner kl. 11 ved pumpehuset Trekanten 1. Her skal der fremvises
              corona pas.{" "}
            </Text>
          </Text>
        </View>

        <View style={styles.middle}>
          <Text style={styles.baseText}>
            Kl. 11.30 <Text style={styles.innerText}> Velkommen!</Text>
          </Text>
        </View>
        <View style={styles.middle}>
          <Text style={styles.baseText}>
            Kl. 11.40{" "}
            <Text style={styles.innerText}>
              Sharks On Board Ny Cirkusforestilling
            </Text>
          </Text>
        </View>
        <View style={styles.middle}>
          <Text style={styles.baseText}>
            Kl. 12.45{" "}
            <Text style={styles.innerText}>
              {" "}
              Vandretur til Gudindeudsigten, hvor der er fællessang med Emil.
            </Text>
          </Text>
        </View>
        <View style={styles.middle}>
          <Text style={styles.baseText}>
            Kl. 14.00{" "}
            <Text style={styles.innerText}> Folkemusik på Stormgården </Text>
          </Text>
        </View>

        <View style={styles.middle}>
          <Text style={styles.baseText}>
            Kl. 14.30{" "}
            <Text style={styles.innerText}>
              {" "}
              Trio Midt(Jylland ) Traditionel nordisk spillemandsmusik
            </Text>
          </Text>
        </View>
        <View style={styles.middle}>
          <Text style={styles.baseText}>
            Kl. 15.30{" "}
            <Text style={styles.innerText}>
              {" "}
              CO2 (Fyn ) Traditionel spillemandsmusik
            </Text>
          </Text>
        </View>
        <View style={styles.middle}>
          <Text style={styles.baseText}>
            Kl. 16.15{" "}
            <Text style={styles.innerText}>
              {" "}
              Balkan damerne (Odense) Traditionel folkemusik fra Balkanlandene
            </Text>
          </Text>
        </View>

        <View style={styles.middle}>
          <Text style={styles.baseText}>
            Kl. 17.00{" "}
            <Text style={styles.innerText}>
              {" "}
              Fynboerne (Fyn) Spillemandslaug m. dansere
            </Text>
          </Text>
        </View>
        <View style={styles.middle}>
          <Text style={styles.baseText}>
            Kl. 17.45{" "}
            <Text style={styles.innerText}>
              {" "}
              Mollevit (Svendborg) Gammel Fynsk folkemusik
            </Text>
          </Text>
        </View>
        <View style={styles.middle}>
          <Text style={styles.baseText}>
            Kl. 18.30{" "}
            <Text style={styles.innerText}>
              Mee`Lads (Fyn) Irsk folkemusik{" "}
            </Text>
          </Text>
        </View>
        <View style={styles.middle}>
          <Text style={styles.baseText}>
            Kl. 19.00{" "}
            <Text style={styles.innerText}>
              Koncert på Håstivals store scene Trekanten 2
            </Text>
          </Text>
        </View>

        <View style={styles.middle}>
          <Text style={styles.baseText}>
            Kl. 20.00{" "}
            <Text style={styles.innerText}>
              ALIVAN Dejlig glad og original danse musik
            </Text>
          </Text>
        </View>

        <View style={styles.middle}>
          <Text style={styles.baseText}>
            Kl. 21.30{" "}
            <Text style={styles.innerText}>
              HOLMEN HUSTLERS Fynsk rap og dancehall fra udkanten{" "}
            </Text>
          </Text>
        </View>
        <View style={styles.last}>
          <Text style={styles.baseText}>
            Kl. 23.00{" "}
            <Text style={styles.innerText}>
              Bitch Muchannon spiller dit lækre track!
            </Text>
          </Text>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default Schedule;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "space-between",
    backgroundColor: "#ffb84d",
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },

  middle: {
    flex: 1,
    marginHorizontal: 1,
    backgroundColor: "#ffb84d",
    borderWidth: 3,
    borderBottomWidth: 0,
  },
  title: {
    backgroundColor: "#ff471a",
    fontSize: 50,
    textAlign: "center",
    borderRadius: 20,
    borderWidth: 2,
    marginHorizontal: 5,
    overflow: "hidden",
    marginBottom: 50,
    fontFamily: "Overlock",
  },
  row: {
    flexDirection: "row-reverse",
  },
  logo: {
    width: 150,
    height: 170,
    top: 10,
    right: "65%",
    marginBottom: 50,
  },
  last: {
    marginHorizontal: 1,
    flex: 1,
    backgroundColor: "#ffb84d",
    borderWidth: 3,
  },

  baseText: {
    fontSize: 20,
    fontWeight: "bold",
    color: "red",
    fontFamily: "Overlock",
  },
  innerText: {
    fontSize: 20,
    color: "black",
    fontFamily: "Overlock",
  },
});
