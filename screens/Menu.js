import React from "react";
import { View, StyleSheet, SafeAreaView, TouchableOpacity } from "react-native";
import { FontAwesome5 } from "@expo/vector-icons";

export default class Menu extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <SafeAreaView>
          <TouchableOpacity
            style={styles.menu}
            onPress={this.props.navigation.openDrawer}
          >
            <FontAwesome5 name="bars" size={24} color="#161924" />
          </TouchableOpacity>
        </SafeAreaView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 0.15,
  },
  menu: {
    alignSelf: "flex-end",
    marginTop: 10,
    paddingHorizontal: 10,
    width: 40,
  },
});
