import * as React from "react";
import {
  StyleSheet,
  Image,
  Text,
  SafeAreaView,
  View,
  ScrollView,
  Pressable,
  Platform,
  StatusBar,
  ImageBackground,
} from "react-native";
import Menu from "./Menu";
import Carousel from "../Carousel/Carousel";
import { Fontisto } from "@expo/vector-icons";
import { useFonts } from "expo-font";

export default HomeScreen = ({ navigation }) => {
  const [loaded] = useFonts({
    Marker: require("../assets/fonts/PermanentMarker_400Regular.ttf"),
    Overlock: require("../assets/fonts/Overlock-Regular.ttf"),
  });

  if (!loaded) {
    return null;
  }

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.row}>
          <Menu navigation={navigation} />
          <View>
            <Image
              style={styles.logo}
              source={require("../assets/HaastivalLogoAlpha.png")}
            />
          </View>
        </View>
        <ImageBackground
          style={styles.background}
          source={require("../assets/HaastivalBack2.jpg")}
        >
          <Text style={styles.title}>SEE YOU IN 2021</Text>
          <Text style={styles.titleDates}>19th June 2021</Text>
          <View style={{ flexDirection: "row" }}>
            <Pressable
              style={styles.buttonReserve}
              onPress={() => navigation.navigate("Reservation")}
            >
              <Fontisto
                style={{ marginHorizontal: 5 }}
                name="ticket"
                size={16}
                color={"#ffd633"}
              />
              <Text style={styles.buttonText}>Reserve Ticket</Text>
            </Pressable>
          </View>
        </ImageBackground>
        <Carousel />
        <ImageBackground
          style={styles.backgroundSchedule}
          source={require("../assets/Schedule.jpg")}
        >
          <View>
            <Pressable
              style={styles.buttonSchedule}
              onPress={() => navigation.navigate("Schedule")}
            >
              <Text style={styles.buttonScheduleText}>Schedule</Text>
            </Pressable>
          </View>
        </ImageBackground>
        <ImageBackground
          style={styles.backgroundAbout}
          source={require("../assets/About.jpg")}
        >
          <View style={{ flexDirection: "row" }}>
            <Pressable
              style={styles.buttonAbout}
              onPress={() => navigation.navigate("About Us")}
            >
              <Text style={styles.buttonAboutText}>About Us</Text>
            </Pressable>
            <Pressable
              style={styles.buttonContact}
              onPress={() => navigation.navigate("Contact Us")}
            >
              <Text style={styles.buttonAboutText}>Contact Us</Text>
            </Pressable>
          </View>
        </ImageBackground>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  background: {
    flex: 1,
    width: "100%",
    height: 600,
    marginBottom: 30,
    alignItems: "center",
  },
  backgroundAbout: {
    flex: 1,
    width: "100%",
    height: 600,
    alignItems: "center",
    marginTop: 45,
  },
  backgroundSchedule: {
    flex: 1,
    width: "100%",
    height: 400,
    alignItems: "center",
  },
  buttonReserve: {
    flexDirection: "row",
    backgroundColor: "rgba(52, 52, 52, 0.0)",
    width: "55%",
    height: 60,
    marginTop: "5%",
    marginHorizontal: 6,
    borderRadius: 50,
    borderWidth: 1.5,
    borderColor: "black",
    justifyContent: "center",
    alignItems: "center",
  },
  buttonAbout: {
    backgroundColor: "rgba(52, 52, 52, 0.0)",
    height: 60,
    width: "45%",
    marginHorizontal: 8,
    marginTop: 40,
    borderRadius: 50,
    borderWidth: 1.5,
    borderColor: "black",
    justifyContent: "center",
    alignItems: "center",
  },
  buttonAboutText: {
    fontSize: 34,
    color: "#ff0000",
    fontFamily: "Overlock",
  },
  buttonContact: {
    backgroundColor: "rgba(52, 52, 52, 0.0)",
    height: 60,
    width: "45%",
    marginTop: 40,
    marginHorizontal: 8,
    borderRadius: 50,
    borderWidth: 1.5,
    borderColor: "black",
    justifyContent: "center",
    alignItems: "center",
  },
  buttonSchedule: {
    backgroundColor: "rgba(52, 52, 52, 0.0)",
    height: 60,
    width: 200,
    marginTop: 40,
    borderRadius: 50,
    borderWidth: 1.5,
    borderColor: "black",
    justifyContent: "center",
    alignItems: "center",
  },
  buttonScheduleText: {
    fontSize: 38,
    color: "#ff0000",
    fontFamily: "Overlock",
  },
  buttonText: {
    fontSize: 28,
    color: "#ffd633",
    fontFamily: "Overlock",
  },
  container: {
    flex: 1,
    backgroundColor: "#ffb84d",
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },
  logo: {
    width: 150,
    height: 170,
    top: 10,
    right: "62%",
    marginBottom: 50,
  },
  row: {
    flexDirection: "row-reverse",
  },
  title: {
    fontFamily: "Marker",
    fontSize: 42,
    marginTop: "20%",
    color: "#ffcc00",
  },
  titleDates: {
    fontFamily: "Marker",
    fontSize: 30,
    color: "#ff0000",
  },
});
