import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { createStackNavigator } from "@react-navigation/stack";
import {
  Feather,
  Fontisto,
  FontAwesome,
  FontAwesome5,
  Entypo,
} from "@expo/vector-icons";
import AboutScreen from "./screens/AboutScreen";
import ContactScreen from "./screens/ContactScreen";
import Router from "./Luchezar/navigation/Router";
import MapScreen from "./screens/MapScreen";
import MyStack from "./screens/Stack";
import Schedule from "./Luchezar/Schedule";
import Food from "./Luchezar/Food";
import Login from "./Soma/Login";
import Logout from "./Soma/Logout";
import AllSharedPictures from "./Vuong/AllSharedPictures";
import RegisterSession from "./Vuong/RegisterSession";
import UploadPictures from "./Vuong/UploadPictures";

const Drawer = createDrawerNavigator();

const MyDrawer = () => (
  <Drawer.Navigator
    drawerStyle={{ backgroundColor: "#ffb84d", width: 230 }}
    drawerPosition="right"
  >
    <Drawer.Screen
      name="Home"
      component={MyStack}
      options={{
        drawerLabel: "Home",
        drawerIcon: ({ tintColor }) => (
          <Feather name="home" size={16} color={tintColor} />
        ),
      }}
    />
    <Drawer.Screen
      name="Reservation"
      component={RegisterSession}
      options={{
        drawerLabel: "Reservation",
        drawerIcon: ({ tintColor }) => (
          <Fontisto name="ticket" size={16} color={tintColor} />
        ),
      }}
    />
    <Drawer.Screen
      name="Map"
      component={MapScreen}
      options={{
        drawerLabel: "Map",
        drawerIcon: ({ tintColor }) => (
          <FontAwesome5 name="map" size={16} color={tintColor} />
        ),
      }}
    />
    <Drawer.Screen
      name="Schedule"
      component={Schedule}
      options={{
        drawerLabel: "Schedule",
        drawerIcon: ({ tintColor }) => (
          <FontAwesome5 name="guitar" size={16} color={tintColor} />
        ),
      }}
    />
    <Drawer.Screen
      name="BandInfo"
      component={Router}
      options={{
        drawerLabel: "Band Information",
        drawerIcon: ({ tintColor }) => (
          <FontAwesome name="group" size={16} color={tintColor} />
        ),
      }}
    />
    <Drawer.Screen
      name="Food and Drinks"
      component={Food}
      options={{
        drawerLabel: "Food and Drinks",
        drawerIcon: ({ tintColor }) => (
          <FontAwesome5 name="hamburger" size={16} color={tintColor} />
        ),
      }}
    />
    <Drawer.Screen
      name="Gallery"
      component={AllSharedPictures}
      options={{
        drawerLabel: "Gallery",
        drawerIcon: ({ tintColor }) => (
          <Fontisto name="picture" size={12} color={tintColor} />
        ),
      }}
    />
    <Drawer.Screen
      name="Upload Pictures"
      component={UploadPictures}
      options={{
        drawerLabel: "Upload Pictures",
        drawerIcon: ({ tintColor }) => (
          <Entypo name="upload-to-cloud" size={16} color={tintColor} />
        ),
      }}
    />

    <Drawer.Screen
      name="About Us"
      component={AboutScreen}
      options={{
        drawerLabel: "About Us",
        drawerIcon: ({ tintColor }) => (
          <Entypo name="info-with-circle" size={16} color={tintColor} />
        ),
      }}
    />
    <Drawer.Screen
      name="Contact Us"
      component={ContactScreen}
      options={{
        drawerLabel: "Contact Us",
        drawerIcon: ({ tintColor }) => (
          <FontAwesome5 name="envelope" size={16} color={tintColor} />
        ),
      }}
    />
    <Drawer.Screen
      name="Logout"
      component={Logout}
      options={{
        drawerLabel: "Logout",
        drawerIcon: ({ tintColor }) => (
          <Entypo name="log-out" size={16} color={tintColor} />
        ),
      }}
    />
  </Drawer.Navigator>
);

const CombinedStack = createStackNavigator();
const Combined = () => (
  <CombinedStack.Navigator
    screenOptions={{ headerShown: false, gestureEnabled: false }}
  >
    <CombinedStack.Screen name="Login" component={Login} />
    <CombinedStack.Screen name="DrawerMenu" component={MyDrawer} />
  </CombinedStack.Navigator>
);
export default function App() {
  return (
    <NavigationContainer>
      <Combined />
    </NavigationContainer>
  );
}
