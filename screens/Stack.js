import * as React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import HomeScreen from "./HomeScreen";
import AboutScreen from "./AboutScreen";
import ContactScreen from "./ContactScreen";
import Schedule from "../Luchezar/Schedule";
import RegisterSession from "../Vuong/RegisterSession";

const Stack = createStackNavigator();

export default MyStack = () => {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="Home" component={HomeScreen} />
      <Stack.Screen name="Contact Us" component={ContactScreen} />
      <Stack.Screen name="About Us" component={AboutScreen} />
      <Stack.Screen name="Schedule" component={Schedule} />
      <Stack.Screen name="Reservation" component={RegisterSession} />
    </Stack.Navigator>
  );
};
