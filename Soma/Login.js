import { StatusBar } from "expo-status-bar";
import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
  SafeAreaView,
} from "react-native";
import * as Facebook from "expo-facebook";
import * as Google from "expo-google-app-auth";

var tryit = true;

export default Login = ({ navigation }) => {
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        <View
          style={{
            marginTop: 20,
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Image
            style={{ width: 150, height: 160 }}
            source={require("./assets/logo.png")}
          />
          <Text
            style={[
              styles.text,
              { marginTop: 10, fontSize: 22, fontWeight: "500" },
            ]}
          >
            Håstival Login
          </Text>
        </View>
        <View
          style={{
            marginTop: 60,
            flexDirection: "row",
            justifyContent: "center",
          }}
        >
          <TouchableOpacity
            onPress={async () => {
              tryit = await FlogIn();
              if (tryit === 1) {
                navigation.navigate("DrawerMenu");
              } else {
                alert("Login Cancelled!");
              }
            }}
          >
            <View style={styles.socialButtonF}>
              <Image
                source={require("./assets/facebook3x.png")}
                style={styles.socialLogo}
              />
              <Text style={styles.text}>Facebook</Text>
            </View>
          </TouchableOpacity>
        </View>
        <Text
          style={[
            styles.text,
            {
              fontSize: 22,
              textAlign: "center",
              marginTop: 50,
              marginVertical: 50,
            },
          ]}
        >
          or
        </Text>
        <View
          style={{
            marginTop: 10,
            flexDirection: "row",
            justifyContent: "center",
          }}
        >
          <TouchableOpacity
            style={styles.socialButtonG}
            onPress={async () => {
              tryit = await signInWithGoogleAsync();
              if (tryit === 0) {
                alert("Login Cancelled!");
              } else {
                navigation.navigate("DrawerMenu");
              }
            }}
          >
            <Image
              source={require("./assets/google3x.png")}
              style={styles.socialLogo}
            />
            <Text style={styles.text}>Google</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const accessToken = async (token) => {
  try {
    await AsyncStorage.setItem(token, GOOGLE_TOKEN);
  } catch (err) {
    throw new Error(err);
  }
};

async function signInWithGoogleAsync() {
  try {
    const { type, accessToken, user } = await Google.logInAsync({
      androidClientId:
        "1076287277894-5ci1a5mroe9k0nqc3vsrjr6csbtltfqr.apps.googleusercontent.com",
      iosClientId:
        "1076287277894-da78mnlso0l4l0njt4uat739pcnl5s4i.apps.googleusercontent.com",
      scopes: ["profile", "email"],
    });
    if (type === "success") {
      console.log(user);
      storeToken(result.accessToken);
      setAccessToken(results.accessToken);
    } else {
      return 0;
    }
  } catch (e) {
    return { error: true };
  }
}

async function FlogIn() {
  try {
    await Facebook.initializeAsync({
      appId: "831000140822692",
    });
    const { type, token, expirationDate, permissions, declinedPermissions } =
      await Facebook.logInWithReadPermissionsAsync({
        permissions: ["public_profile"],
      });
    if (type === "success") {
      // Get the user's name using Facebook's Graph API
      console.log("logged in to Facebook");
      const response = await fetch(
        `https://graph.facebook.com/me?access_token=${token}`
      );
      return 1;
    } else {
      // type === 'cancel'
    }
  } catch ({ message }) {
    alert(`Facebook Login Error: ${message}`);
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffb84d",
    paddingHorizontal: 30,
  },
  text: {
    fontFamily: "Avenir Next",
    color: "#1D2029",
    textAlign: "center",
    marginTop: 15,
  },
  socialButtonF: {
    flexDirection: "row",
    marginHorizontal: 12,
    paddingVertical: 12,
    paddingHorizontal: 30,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: "rgba(171, 180, 189, 0.65)",
    borderRadius: 4,
    backgroundColor: "#fff",
    shadowColor: "rgba(171, 180, 189, 0.35)",
    shadowOffset: { width: 0, height: 10 },
    shadowOpacity: 1,
    shadowRadius: 20,
    elevation: 5,
  },
  socialButtonG: {
    flexDirection: "row",
    marginHorizontal: 12,
    paddingVertical: 12,
    paddingHorizontal: 37,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: "rgba(171, 180, 189, 0.65)",
    borderRadius: 4,
    backgroundColor: "#fff",
    shadowColor: "rgba(171, 180, 189, 0.35)",
    shadowOffset: { width: 0, height: 10 },
    shadowOpacity: 1,
    shadowRadius: 20,
    elevation: 5,
  },
  socialLogo: {
    width: 48,
    height: 48,
    marginRight: 24,
  },
  link: {
    color: "#FF1654",
    fontSize: 14,
    fontWeight: "500",
  },
});
