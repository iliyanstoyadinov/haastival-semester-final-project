import * as React from "react";
import DisplayAnImage from "../BandInfo";
import "react-native-gesture-handler";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import Band1 from "../Band1";
import Band2 from "../Band2";
import Band3 from "../Band3";
import Band4 from "../Band4";
import Schedule from "../Schedule";
import Food from "../Food";

const Stack = createStackNavigator();

const Router = (props) => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name={"Band Info"}
        component={DisplayAnImage}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name={"Band1"}
        component={Band1}
        options={{
          title: "ALIVAN",
        }}
      />
      <Stack.Screen
        name={"Band2"}
        component={Band2}
        options={{
          title: "Bitch Muchannon",
        }}
      />
      <Stack.Screen
        name={"Band3"}
        component={Band3}
        options={{
          title: "HOLMEN HUSTLERS",
        }}
      />
      <Stack.Screen
        name={"Band4"}
        component={Band4}
        options={{
          title: "Sharks On Board Ny ",
        }}
      />
      <Stack.Screen
        name={"Schedule"}
        component={Schedule}
        options={{
          title: "Event Schedule",
        }}
      />

      <Stack.Screen
        name={"Food"}
        component={Food}
        options={{
          title: "Food&Drinks",
        }}
      />
    </Stack.Navigator>
  );
};
export default Router;
