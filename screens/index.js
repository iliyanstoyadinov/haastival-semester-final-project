import * as React from "react";
import Menu from "./Menu";

export const ProfileScreen = ({ navigation }) => (
  <Menu navigation={navigation} name="Profile" />
);

export const TicketScreen = ({ navigation }) => (
  <Menu navigation={navigation} name="Ticket" />
);

export const MapScreen = ({ navigation }) => (
  <Menu navigation={navigation} name="Map" />
);

export const ScheduleScreen = ({ navigation }) => (
  <Menu navigation={navigation} name="Schedule" />
);

export const FoodScreen = ({ navigation }) => (
  <Menu navigation={navigation} name="Food" />
);

export const PictureShareScreen = ({ navigation }) => (
  <Menu navigation={navigation} name="PictureShare" />
);
