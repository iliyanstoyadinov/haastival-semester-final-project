import * as React from "react";
import {
  StyleSheet,
  Image,
  Text,
  SafeAreaView,
  View,
  ScrollView,
  Pressable,
  Platform,
  StatusBar,
} from "react-native";
import Menu from "./Menu";
import * as Linking from "expo-linking";
import Back from "./Back";

export default ContactScreen = ({ navigation }) => {
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.row}>
          <Menu navigation={navigation} />
          <View>
            <Image
              style={styles.logo}
              source={require("../assets/HaastivalLogoAlpha.png")}
            />
          </View>
          <Back navigation={navigation} />
        </View>

        <Text style={styles.contact}>Contact Us </Text>
        <View style={styles.contactContainer}>
          <Text style={styles.contactText}>
            If you need more information, have questions or anything else, just
            get in touch. {"\n"}
            You can contact us by phone, e-mail or by filling out the form.{" "}
            {"\n"}
            For ongoing information about program and activities find us on:{" "}
            {"\n"}
            <Pressable
              onPress={() =>
                Linking.openURL("https://www.facebook.com/haastival/?fref=ts")
              }
            >
              <Text style={styles.facebook}>Facebook</Text>
            </Pressable>{" "}
            {"\n"}
            We will respond as soon as possible. {"\n"}
            {"\n"}
            Håstival {"\n"}
            Trekanten 2, Håstrup {"\n"}
            5600 Fåborg {"\n"}
            {"\n"}
            Write to {"\n"}
            <Pressable
              onPress={() => Linking.openURL("mailto: haastival@gmail.com")}
            >
              <Text style={styles.facebook}>haastival@gmail.com</Text>
            </Pressable>{" "}
            {/* <Pressable onPress={() => Linking.openURL("mobilepay://app")}>
              <Text>mobilePay</Text>
            </Pressable>{" "} */}
          </Text>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  contact: {
    backgroundColor: "#ff471a",
    fontSize: 50,
    textAlign: "center",
    borderRadius: 20,
    borderWidth: 2,
    marginHorizontal: 5,
    overflow: "hidden",
    marginBottom: 40,
    fontFamily: "Overlock",
  },
  contactText: {
    marginHorizontal: 10,
    fontSize: 20,
    marginBottom: 10,
    color: "brown",
    marginHorizontal: 15,
    fontFamily: "Overlock",
  },
  container: {
    flex: 1,
    backgroundColor: "#ffb84d",
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },
  contactContainer: {
    borderRadius: 20,
    borderWidth: 1,
    marginBottom: 60,
    marginHorizontal: 5,
  },
  facebook: {
    fontSize: 20,
    color: "blue",
    textDecorationLine: "underline",
    fontFamily: "Overlock",
  },
  logo: {
    width: 150,
    height: 170,
    top: 10,
    right: "65%",
    marginBottom: 50,
  },
  row: {
    flexDirection: "row-reverse",
  },
});
