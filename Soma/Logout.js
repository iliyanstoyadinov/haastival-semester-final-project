import { StatusBar } from "expo-status-bar";
import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
  SafeAreaView,
} from "react-native";
import * as Facebook from "expo-facebook";
import * as Google from "expo-google-app-auth";
import Menu from "../screens/Menu";

export default Logout = ({ navigation }) => {
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        <View style={styles.row}>
          <Menu navigation={navigation} />
          <View>
            <Image
              style={styles.logoTitle}
              source={require("../assets/HaastivalLogoAlpha.png")}
            />
          </View>
        </View>
        <View
          style={{
            marginTop: 60,
            flexDirection: "row",
            justifyContent: "center",
          }}
        >
          <TouchableOpacity
            onPress={async () => {
              await FlogOut();
              navigation.navigate("Login");
            }}
          >
            <View style={styles.socialButtonF}>
              <Image
                source={require("./assets/facebook3x.png")}
                style={styles.socialLogo}
              />
              <Text style={styles.text}>Facebook logout</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View
          style={{
            marginTop: 10,
            flexDirection: "row",
            justifyContent: "center",
          }}
        >
          <TouchableOpacity
            style={styles.socialButtonG}
            onPress={async () => {
              await logOutWithGoogleAsync();
              navigation.navigate("Login");
            }}
          >
            <Image
              source={require("./assets/google3x.png")}
              style={styles.socialLogo}
            />
            <Text style={styles.text}>Google logout</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const accessToken = async (token) => {
  try {
    await AsyncStorage.setItem(token, GOOGLE_TOKEN);
  } catch (err) {
    throw new Error(err);
  }
};

async function logOutWithGoogleAsync() {
  await Google.logOutAsync({
    accessToken,
    androidClientId:
      "1076287277894-5ci1a5mroe9k0nqc3vsrjr6csbtltfqr.apps.googleusercontent.com",
    iosClientId:
      "1076287277894-da78mnlso0l4l0njt4uat739pcnl5s4i.apps.googleusercontent.com",
  });
  console.log("logged out of google");
}

async function FlogOut() {
  await Facebook.logOutAsync();
  console.log("logged out of facebook");
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffb84d",
    paddingHorizontal: 30,
  },
  text: {
    fontFamily: "Avenir Next",
    color: "#1D2029",
    textAlign: "center",
    marginTop: 15,
  },
  row: {
    flexDirection: "row-reverse",
  },
  logoTitle: {
    width: 150,
    height: 170,
    top: 10,
    right: "65%",
    marginBottom: 50,
  },
  socialButtonF: {
    flexDirection: "row",
    marginHorizontal: 12,
    paddingVertical: 12,
    paddingHorizontal: 30,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: "rgba(171, 180, 189, 0.65)",
    borderRadius: 4,
    backgroundColor: "#fff",
    shadowColor: "rgba(171, 180, 189, 0.35)",
    shadowOffset: { width: 0, height: 10 },
    shadowOpacity: 1,
    shadowRadius: 20,
    elevation: 5,
  },
  socialButtonG: {
    flexDirection: "row",
    marginHorizontal: 12,
    paddingVertical: 12,
    paddingHorizontal: 38,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: "rgba(171, 180, 189, 0.65)",
    borderRadius: 4,
    backgroundColor: "#fff",
    shadowColor: "rgba(171, 180, 189, 0.35)",
    shadowOffset: { width: 0, height: 10 },
    shadowOpacity: 1,
    shadowRadius: 20,
    elevation: 5,
    marginTop: 100,
  },
  socialLogo: {
    width: 48,
    height: 48,
    marginRight: 24,
  },
  link: {
    color: "#FF1654",
    fontSize: 14,
    fontWeight: "500",
  },
});
