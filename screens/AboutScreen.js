import * as React from "react";
import {
  StyleSheet,
  Image,
  Text,
  SafeAreaView,
  View,
  ScrollView,
  Platform,
  StatusBar,
} from "react-native";
import Menu from "./Menu";
import AboutCarousel from "../Carousel/AboutCarousel";
import Back from "./Back";

export default AboutScreen = ({ navigation }) => {
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.row}>
          <Menu navigation={navigation} />
          <View>
            <Image
              style={styles.logo}
              source={require("../assets/HaastivalLogoAlpha.png")}
            />
          </View>
          <Back navigation={navigation} />
        </View>
        <Text style={styles.about}>About Us </Text>
        <View style={styles.storyContainer}>
          <Text style={styles.story}>Our Story:</Text>
          <Text style={styles.storyText}>
            {"\u25CF"} Håstival started with a walk in our backyard of about 6
            acres of land. What are we going to use all that space for? - We
            asked each other.{"\n"}
            {"\u25CF"} We can make a festival! As thought so done.{"\n"}
            {"\u25CF"} Håstrup is a very special town with very special people,
            so many good forces flowed in from volunteers, so that all the
            dreams we created together, could be brought to life.{"\n"}
            {"\u25CF"} Our first and most important goal is that the whole event
            is carried by desire, to make it happen.{"\n"}
            {"\u25CF"} Money must not limit anyone from being involved or to
            determine our success. Everyone can participate and help to create
            community.{"\n"}
            {"\u25CF"} Something as simple as having fun and being filled with
            great experiences is our highest priority.{"\n"}
          </Text>
        </View>
        <View style={styles.keyWordsContainer}>
          <Text style={styles.keyWords}>Our Key Words:</Text>
          <Text style={styles.keyWordsText}>
            Fun {"\n"}
            Non Profit{"\n"}
            Happy Days{"\n"}
            Original Music {"\n"}
            Community for All{"\n"}
          </Text>
        </View>
        <Text style={styles.title}>Håstivals Board of Directors </Text>
        <AboutCarousel />
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  about: {
    backgroundColor: "#ff471a",
    fontSize: 50,
    textAlign: "center",
    borderRadius: 20,
    borderWidth: 2,
    marginHorizontal: 5,
    overflow: "hidden",
    marginBottom: 80,
    fontFamily: "Overlock",
  },
  container: {
    flex: 1,
    backgroundColor: "#ffb84d",
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },
  logo: {
    width: 150,
    height: 170,
    top: 10,
    right: "65%",
    marginBottom: 50,
  },
  keyWords: {
    fontSize: 40,
    textAlign: "center",
    borderColor: "black",
    marginHorizontal: 50,
    marginBottom: 20,
    fontFamily: "Overlock",
  },
  keyWordsContainer: {
    borderRadius: 20,
    borderWidth: 1,
    marginBottom: 60,
    marginHorizontal: 5,
  },
  keyWordsText: {
    marginHorizontal: 10,
    fontSize: 30,
    textAlign: "center",
    marginBottom: 10,
    color: "brown",
    marginHorizontal: 15,
    fontFamily: "Overlock",
  },
  row: {
    flexDirection: "row-reverse",
  },
  story: {
    fontSize: 40,
    textAlign: "center",
    borderColor: "black",
    marginHorizontal: 100,
    marginBottom: 20,
    fontFamily: "Overlock",
  },
  storyContainer: {
    borderRadius: 20,
    borderWidth: 1,
    marginBottom: 60,
    marginHorizontal: 5,
  },
  storyText: {
    marginHorizontal: 10,
    fontSize: 20,
    color: "brown",
    marginHorizontal: 15,
    fontFamily: "Overlock",
  },
  title: {
    marginHorizontal: 5,
    marginBottom: 20,
    fontSize: 44,
    textAlign: "center",
    borderColor: "black",
    borderRadius: 20,
    borderWidth: 2,
    fontFamily: "Overlock",
  },
});
