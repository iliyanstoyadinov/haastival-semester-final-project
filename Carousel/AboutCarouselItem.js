import React from "react";
import {
  View,
  StyleSheet,
  Text,
  Image,
  useWindowDimensions,
} from "react-native";

export default CarouselItem = ({ item }) => {
  const { width } = useWindowDimensions();
  return (
    <View style={[styles.container, { width }]}>
      <Image
        source={item.image}
        style={[styles.image, { width, resizeMode: "stretch" }]}
      />
      <View>
        <Text style={styles.title}>{item.title}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  image: {
    flex: 0.7,
    height: Platform.OS === "android" ? 500 : 600,
    justifyContent: "center",
  },
  title: {
    flex: 0.1,
    marginTop: 34,
    fontWeight: "bold",
    fontSize: 24,
    textAlign: "center",
    fontFamily: "Overlock",
  },
});
