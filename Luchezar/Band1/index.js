import React, { Component } from "react";
import {
  Linking,
  View,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  SafeAreaView,
} from "react-native";

const Band1 = () => {
  return (
    <SafeAreaView style={styles.container}>
      <Text style={styles.textst}>ALIVAN </Text>
      <Image style={styles.logo} source={require("./Band1.png")} />
      <Text style={styles.textsm}>Social media:</Text>
      <View style={[styles.fb, { flexDirection: "row", flexWrap: "wrap" }]}>
        <TouchableOpacity
          activeOpacity={0.5}
          onPress={() => Linking.openURL(url)}
        >
          <Image style={styles.log} source={require("./fb.webp")} />
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={1}
          onPress={() => Linking.openURL(url1)}
        >
          <Image style={styles.log} source={require("./sp.png")} />
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};
const url = "https://www.facebook.com/officialalivan";
const url1 =
  "https://open.spotify.com/album/5sFGOwe8Ne76U3XbuieIHM?si=R5EQlOqcR061d0u6VBh1TQ&nd=1";
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: "10%",
    backgroundColor: "#ffb84d",
    paddingBottom: "10%",
  },
  logo: {
    width: 320,
    height: 280,
    marginLeft: "10%",
    marginTop: "10%",
  },
  textst: {
    fontSize: 60,
    textAlign: "center",
    fontFamily: "Overlock",
  },
  textsm: {
    marginLeft: "2%",
    marginTop: "2%",
    fontFamily: "Overlock",
    fontSize: 30,
    textAlign: "left",
  },
  log: {
    width: 60,
    height: 60,
    marginLeft: "5%",
    marginTop: "5%",
  },
  fb: {
    margin: "5%",
  },
});

export default Band1;
